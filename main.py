#TODO:
#   ERRORS TO FIX:
#		- Equalization RGB images -> HSV
#       - Geometric Mean is not working
#       - Gaussian Bandreject has a bad behavior
#       - Some image does not open
#       - High exponent values has strange bahviour in power function
#       - Close plot window block a new window generation


#Imports
import numpy   as np #Numpy
import sys
import os

sys.path.append('modules/')

from soin_image import soin_image
from soin_gui import soin_gui
from image_processor import image_processor, filter_processor, fourier
from huffman import huffman
from binary_file import binary_file
from wavelet_haar_tranform import haar

#################
### UNIT TEST ###
#################

#Filter Processor
#filt_proc = filter_processor()
#
##Fourier Init
#fourier_hand = fourier()
#
##Huffman Init
#huffman_hand = huffman()
#
##Haar Init
#haar_hand = haar()
#
##Image Processor Init
#img_proc      = image_processor(filt_proc, fourier_hand, huffman_hand,haar_hand)
#
##Init Image
##soin_img_hand = soin_image(r"image_examples/general/lena_color_512.tif", img_proc)
#soin_img_hand = soin_image(r"image_examples/compress_benchmark/benchmark.bmp", img_proc)
##soin_img_hand = soin_image(r"cameraman_256.tif", img_proc)
#soin_img_hand.open_image()
#
##Showing Original
#soin_img_hand.show_image()
#
#p = img_proc.dhwt_func(soin_img_hand.img_array, 1)
##print(p+70)
#p = img_proc.normalize(p).astype('uint8')
#
##p = ((p/255)*254.9375)-70
#
##print(p)
#
##comp, origin = img_proc.haar_separate_components(img_proc.normalize(p).astype('uint8'),1)
##result = img_proc.haar_join_components(comp, origin,1)
##p = img_proc.rgb2gray(p, 0)
##p = img_proc.gray2rgb(p)
##print(p.shape)
#i = img_proc.idhwt_func(p, 1)
#
##Updating and showing processed image
#soin_img_hand.img_array = i#img_proc.normalize(result).astype('uint8');
#soin_img_hand.update_image()
#soin_img_hand.show_image()

#################
###APPLICATION###
#################

def run():
	#Filter Processor
	filt_proc = filter_processor()
	
	#Fourier Init
	fourier_hand = fourier()

	#Huffman Init
	huffman_hand = huffman()

	#Haar Init
	haar_hand = haar()
	
	#Bin File
	bin_file_hand = binary_file()

	#Image Processor Init
	img_proc      = image_processor(filt_proc, fourier_hand, huffman_hand, haar_hand)
	
	##Image Init
	soin_img_hand = soin_image("image_examples/general/lena_gray_256.tif", img_proc)
	soin_img_hand.open_image()
	
	#Gui init
	current_path = os.path.dirname(os.path.abspath(__file__))
	soin_gui_hand = soin_gui(None, img_proc, bin_file_hand, current_path)
	soin_gui_hand.build_wm()
	soin_gui_hand.run_wm()

run()